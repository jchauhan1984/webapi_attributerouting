﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI_AttributeRouting.Models;

namespace WebAPI_AttributeRouting.Controllers
{
    //[RoutePrefix("api/students")]
    public class StudentsV1Controller : ApiController
    {
         static List<StudentV1> students = new List<StudentV1>()
        {
            new StudentV1() { Id = 1,Name = "Tome"},
            new StudentV1() { Id = 2,Name = "Sam"},
            new StudentV1() { Id = 3,Name = "John"},
        };

        [Route("~/api/teachers")]
        public IEnumerable<Teacher> GetTeachers()
        {
            List<Teacher> teachers = new List<Teacher>()
            {
                new Teacher(){Id = 1, Name = "Rob" },
                new Teacher(){Id = 2, Name = "Mike" },
                new Teacher(){Id = 3, Name = "Mary" }
            };
            return teachers;
        }


        public IEnumerable<StudentV1> Get()
        {
            return students;
        }


        public StudentV1 Get(int id)
        {
            return students.FirstOrDefault(x => x.Id == id);
            //if (student == null)
            //{
            //    return Content(HttpStatusCode.NotFound, "Student data not found");
            //}
            //else
            //{
            //    return Ok(student);
            //}
        }

        [Route("{name:alpha}")]
        public StudentV1 Get(string name)
        {
            return students.FirstOrDefault(s => s.Name.ToLower() == name.ToLower());
        }

        [Route("{id}/courses")]
        public IEnumerable<string> GetStudentCourses(int id)
        {
            if (id == 1)
                return new List<string>() { "C#", "ASP .Net", "SQL Server" };
            else if (id == 2)
                return new List<string>() { "Java", "ASP .Net", "SQL Server" };
            else
                return new List<string>() { "Python", "ASP .Net", "SQL Server" };
        }

        [Route("")]
        public HttpResponseMessage Post(StudentV1 student)
        {
            students.Add(student);
            var response = Request.CreateResponse(HttpStatusCode.Created);
            response.Headers.Location = new Uri(Url.Link("GetStudentById", new { id = student.Id }));
            return response;
        }
}
}
